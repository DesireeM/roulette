import java.util.Scanner;
public class Roulette
{
    public static void main (String [] args)
    {
        RouletteWheel rw = new RouletteWheel();
        
        int money = 1000;

        Scanner scan = new Scanner(System.in);
        System.out.println("Do you want to make a bet? Answer with yes or no: ");
        String userAnswer = scan.nextLine();

        if (userAnswer.equals("yes"))
        {
            System.out.println("What number would you'd like to bet on?: ");
            int userNumber = scan.nextInt();

            while (userNumber < 0 || userNumber > 36)
            {
                System.out.println("Please enter a number between 0 to 36");
                userNumber = scan.nextInt();
            }

            System.out.println("How much do you want to bet?: ");
            int userBet = scan.nextInt();

            while (userBet > money || userBet < 1)
            {
                System.out.println("Please enter a bet between 0-1000$ ?: ");
                userNumber = scan.nextInt();
            }

            rw.spin();
            int spinnedNumber = rw.getValue();

            System.out.println("The number is . . . " +spinnedNumber);

            if(spinnedNumber == userNumber)
            {
                userBet = userBet*35;
                System.out.println("Good job! You won " + userBet +"$");
                money += userBet;
            }

            else
            {
                System.out.println("You lost "+userBet + "$");
                money -= userBet;
            }
        }
    }
}