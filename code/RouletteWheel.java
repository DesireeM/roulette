import java.util.Random;
public class RouletteWheel 
{
  private Random random= new Random();
  private int number = 0;

  public void spin()
  {
    number = random.nextInt(37);
  }

  public int getValue()
  {
    return number;
  }
}
